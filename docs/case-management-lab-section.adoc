Credit Card Transactions are evaluated by an automatic system that detect fraudulent activities:
a fraudulent activity can be triggered by one or more transactions that are considered suspicious.
When a fraudulent activity is detected a new case instance is created to manage the human driven investigation phase.

The investigation has two stages:


1. *Triage* that contains two user tasks: _First Evaluation_ and then _Call Customer for Confirmation_ that happens if the evaluation outcome is suspect.

2. *Follow up* that contains three user tasks: _Incoming Customer Call_, _Merchant Check_, _Legal Prosecution_. Those tasks are not linked to each other and can be triggered once or more time, based on the Fraud Analyst decisions or on external events. In fact, the task Incoming Customer Call is triggered when a customer call reaches the CRM.

The user task are assigned to the following case roles: fraud-analyst, crm and legal. They can be assigned at start time and changed during the case life (for demo purposes, if at start time a user is not specified for the role, the user starting the case will be assigned to that role).

During the case life the following milestones can be achieved:

* *Suspect:* when the fraud analyst validate the automatic detection, the card is temporary blocked in order to avoid further frauds and the CRM is in charge of calling the customer to definitively confirm the fraud.

* *Confirmed:* when the customer confirm the fraudulent activity the card is permanently blocked. Then the following milestones are enabled.

* *Reissue:* when the customer ask for a reissue of a new credit card. It's worth noticing that the customer can ask for a reissue when he confirm the fraud or later when s/he calls back.

* *Reimbursement:* whether the customer provides documentation of the complaint filed with the local authorities, s/he is eligible to receive a reimbursement.

The milestones are data driven, so there's no strict predefined sequence. E.g. Card _reissue_ usually happens immediately, whereas the _reimbursement_ requires more time, because the customer has to file a complaint, but it could happen that the customer does not ask for a _reissue_ whereas s/he wants the _reimbursement_.

=== Case Management Walk-through 

To explore the features of the case management, you are going to use two different web applications:

* *Sample Show Case Application* to start a new case instance, to inspect the stages and milestones, to trigger the optional case fragments.
* *Business Central*: particularly, the task list to perform the tasks and the process instance to inspect the BPMN diagram at runtime.


CAUTION: The *Sample Show Case Application* is a useful tool to explore the case management features. Nevertheless, it's not meant to be a full functional application. The final user interface of a case management application can have really diverse requirements, the look and feel can be different but even the navigation and the organization of the data in the page; for example, the case view is likely to offer a way to access directly to the task list. We expect that our customers will develop their own application and use this sample at early stages of the development to probe the main features of the engine.

. Start the case

.. Open the *Sample Show Case Application* and log in
+
WARNING: add the URL

.. Select the *start case* button
+ 
A pop up window lets you specify the `case owner` and the other `case roles`. This case model involves three roles: *fraud analyst*, *crm* and *legal*. For the sake of simplicity, you don't need to insert anything, because the back end logic assigns the current user for all the roles. The key point is that you can assign a case role at the initialization time or to update them later, during the case life.
+
.Sample Show Case Application
image::docs/images/cm-sample-case-app.png[]

.. Select *start* button
+
A new case instance is created, you should see a new line appearing in the case list.
+
.New case instance
image::docs/images/cm-case-instance.png[]

. Explore the case instance

.. *Click* the case instance to see the case status
+
.Case instance details
image::docs/images/cm-case-details.png[]
+
In this view there are many useful information, particularly:

... The *stages* chevron shows in _blue_ the *active* stage, in _white_ the *available* stages, in _grey_ the *completed* stages.

... The *available actions* box show the actions that can be initiated by the case owner. The reader can try to create a task on the fly using *new user task* menu. (Click start on the kebab menu, i.e. the three vertical dots)

... *Milestones* box shows the available milestone, as they are met the icon is changed in check mark.

... *Roles* shows the case roles for a case and let the user modify them during the case life. Case roles provide an abstraction layer for user participation in case handling. A user task can be assigned to a role instead of a user or a group, so the case owner has the chance to assign the right set of users or groups for a specific case instance.

. Explore the case instance in the *BPMN diagram* _(technical view)_
+
It's likely that for a _business user_ the information provided at previous step are more then enough to understand the process status. For people that understand the BPMN notation, it's even interesting to track the execution layered on top of the BPMN diagram.

.. Open the *Business Central* console

.. From the top bar, select *Menu* and then *Process Instances* (in the _Manage_ section)
+
.Process Instances Menu
image::docs/images/cm-process-instances.png[]

.. Select the *process instance*
+
A case instance is a process instance with some extra features, so in the process administration console you can list it as a normal process.
+
.Process Instances List
image::docs/images/cm-process-instance.png[]

.. Select the *Diagram* tab and look over the BPMN picture
+
* the *start* node was executed (greyed out)
* the *Triage* stage is active (highlighted in red), the stage in BPMN is an _ad-hoc sub process_
* the task *First Evaluation* is active. It is has been started at the stage activation because it has the property `ad hoc autostart` flagged (check it later)
* the milestone *suspect* is active: if the milestone condition will be met the milestone will be completed and the linked tasks will be performed. It is worth noticing that, after the `Temporary Card Block` (just a mock up of an external service call), there is a signal named `Confirmed`, this is the way to activate the next milestone.
+
.Process Diagram
image::docs/images/cm-process-instance-bpmn.png[]

. Run the *Triage* stage
+
In this stage the case behaves like a _"traditional process"_ where the sequence of tasks is strictly prescribed by the process diagram. In fact, the first task, _First Evaluation_, is followed by a gateway that based on certain conditions triggers the task _Call Customer for Confirmation_; finally, the stage is completed.

.. Perform the *First Evaluation* task

... In *Business Central*, select *task inbox*
+
image::docs/images/cm-task-inbox.png[] 

... Click over *First Evaluation*: the form shows the _Suspicious Activity_ that triggered the case instance. _Risk Ranking_ comes from the automatic assessment done through the DMN logic.  In the _Transactions_ section, there is the list of transactions under evaluation.

... At the bottom of the form click *Start*

... Playing the _Fraud Analyst_ role, based on your experience, _confirm_ the suspect.
+
Click *confirm* radio button and then the *Complete* button   

.. Perform the *Call Customer for Confirmation* task

... From the task inbox, select *Call Customer for Confirmation*

... At the bottom of the form click *Start*

... Playing the _CRM_ role: call the customer (card holder) and ask if she / he recognizes the suspicious transactions, the customer confirms that she / he didn't make those payments, but at the moment s/he does not want a new card.
+
Click *confirm* radio button and then the *Complete* button.

. (_Optionally_) Open the BPMN diagram to check the execution over it.

.. Note that the *Triage* stage is completed.

.. Note that the milestones *Suspect* and *Confirmed* were triggered and the subsequent tasks were executed.

. Run the *Follow up* stage
+
Trigger the _optional tasks_
+
Switch back to the *Sample Show Case Application*. Click the refresh icon on the top right of the case instance page
+
.Refresh case instance details
image::docs/images/cm-refresh.png[]
+
Check the completed milestones (suspect and confirmed)

.. Trigger *Incoming Customer Call*

.. Trigger *Merchant Check*

.. Optionally, trigger the *Legal Prosecution*

.. Perform the *Incoming Customer Call* task

.. Perform the *Merchant Check* task

. Check the case status and close it

=== Understand the project

The case management project requires some specific configurations.

=== Enhance the case management project

In this section, you will extend the existing case definition to introduce a new *milestone* and a *decision* logic.

1. Create a DMN to decide whether *Legal Prosecution* makes sense.

2. Create the Test Scenario to verify the decision logic

3. Add the decision logic before the *Legal Prosecution* user task

4. Create the new milestone *False Positive*
+
When there's no fraud, a feedback is sent back to the automatic detection system in order to tune up the detection algorithm.