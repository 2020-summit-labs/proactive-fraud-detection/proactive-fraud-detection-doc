:data-uri:
:toc: left
:sectanchors: true
:sectlinks: true
:sectnums: true
:encoding: UTF-8

== **Decision Modelling Notation (DMN) Lab Overview**

=== Decision Modelling Notation (DMN) Background

NOTE: Decision Model and Notation (DMN) is a standard published by the Object Management Group.[1] It is a standard approach for describing and modeling repeatable decisions within organizations to ensure that decision models are interchangeable across organizations. +
*Reference* https://en.wikipedia.org/wiki/Decision_Model_and_Notation +

The DMN standard provides the industry with a modeling notation for decisions that will support decision management and business rules. The notation is designed to be readable by business and IT users alike. This enables various groups to effectively collaborate in defining a decision model:

- The business people who manage and monitor the decisions +
- The business analysts or functional analysts who document the initial decision requirements and specify the detailed decision models and decision logic +
- The technical developers responsible for the automation of systems that make the decisions. +
- The DMN standard can be effectively used standalone but it is also complementary to the BPMN and CMMN standards. +

=== The DMN Model Reference 

NOTE: A full whitepaper containing an introduction to DMN can be found here: https://www.omg.org/news/whitepapers/An_Introduction_to_Decision_Modeling_with_DMN.pdf 

.DMN context diagram
image::docs/images/DMN-Context.png[Overview, 500,200]

A DMN model contains a standard design palette to express the decision / rule requirements:

- *Decisions* : Shown as simple rectanglur boxes that contain the name of the decision. In effect nodes in the model where one or several inputs determine an output based on decision logic. A DMN model can contain one or more Decisions.
- *Business Knowledge Model* : Reusable pieces of decision logic. Decisions that have the same logic but depend on different sub-inputs or sub-decisions use business knowledge models to determine which procedure to follow. +
- *Knowledge Sources* : External regulations, documents, committees, policies, and so on that shape decision logic. Knowledge sources are references to real-world factors rather than executable business rules. +
- *Input data* : Information necessary to determine a decision. This information usually includes business-level concepts or objects relevant to the business, such as a restaurant’s peak business hours and staff availability.

===  DMN Lab Solution Artefacts

The lab comes with the following pre-built solution assets : 

- *DMN Model* - Contains the actual rule definition +
- *DMN Test Scenarios* - Contains all of the test scenarios and test data used to validate the rule +
- *DMN Rest API* - >>TBC<< +

=== The Card Fraud DMN Model 

The Fraud Model starts off looking as follows 

.Initial DMN model design
image::docs/images/dmn-initial-model.png[Overview, 750,400]

At the end of the lab the fraud model show appear as follows: 

.Completed DMN model design
image::docs/images/DMN-Model.png[Overview, 750,400]


=== The Card Fraud DMN Model Explained
- *Last Transaction* : This is "DMN Decision" node containing a literal expression that uses the reverse(list) Feel function to invert the list of transactions and to get a hanlde on the latest transaction.  +
- *Transactions* : This is an "DMN input data" node that basically defines a list of transactions as the datatype.
- *Merchant Blacklist* : This is "DMN Decision" node containing a Relation expression type that captures the list of blacklisted merchants.
+
NOTE: *Relation Expression Type* : Is a table of value expresssions that are often used in zero input decisions to embed static data tables inside the decision model.
+
- *Risk Rating For Blacklisted Merchant* : This is a "DMN Business Knowledge Model(BKM)" node which checks the list of transactions to check if any of the transactions contain a blacklisted merchanted. If a blacklisted merchant is found the risk score for the collection of transactions is set to '3' which means high-risk.  +
+
NOTE: *DMN Business Knowledge Model* : Contains a reusable function with one or more decision elements. Decisions that have the same logic but depend on different sub-input data or sub-decisions use business knowledge models to determine which procedure to follow.
+
- *Risk Rating For Last Transaction Amount* : This is another "DMN Business Knowledge Model(BKM)" node that uses a decision table to determine the risk rating for the transactions. The decision table uses the Card Type, Location and Amount values to calculate the risk rating. The decision table uses the "Unique" hit policy.
+
- *Risk Rating For Last 24 Hours Transactions* : This is another "DMN Business Knowledge Model (BKM)" node that uses a decision table to determine the risk rating based on the total amount of spend within the last 24hrs. 
+
- *Risk Rating For Denied Transactions* : This is a "DMN Business Knowledge Model (BKM)" node which uses a literal expression to interate through the list of transactions to check if any of the transactions have been previosly denied. If a previous denied transaction  is found the risk score for the collection of transactions is set to '3' which means high-risk.
+ 
- *Total Amount from Last 24 Hours* : This is a "DMN Decision" node containing a context expression which is used to determine the list of transactions and transaction values for the  last 24 hours. The output of this node is passed into the final "Risk Score" node and its is also referenced by the *Risk Rating For Last 24 Hours Transactions* BKM node.    
+
NOTE: *Context Expressions* : Context expression in DMN is a set of variable names and values with a result value. Each name-value pair is a context entry. You use context expressions to represent data definitions in decision logic and set a value for a desired decision element within the DMN decision model. A value in a boxed context expression can be a data type value or FEEL expression, or can contain a nested sub-expression of any type, such as a decision table, a literal expression, or another context expression. 
+
- *Risk Score* : This is a "DMN Decision" node that contains a context expression which takes input from the other decision rule and BKM nodes to determine the final risk rate. 

=== DMN Lab Instructions

==== DMN Lab 1: Build Deploy Test
- Access the Business Central Web based user interface.


NOTE:  *Business Central URL* : xyz +
*Business Central Username*  +
*Business Central Password* 

. From the main menu select the blue "project" link which is just below the Design icon.
.. You should now be presented will a list of Spaces. Select the space called "MySpace"..
.. You should now be presented with a list of projects. Select the project called "fraudmanagement-dmn-bc".
.. You should now be presented with a project containing three assets:
... Fraud-Scoring (DMN Model)
... ScenariojunuitActivatorTest (Data Objects)
... Test-Scenairio (Test Scenarios)
+
.Project asset overview
image::docs/images/dmn-project-assets.png[Overview, 700, 244]

==== DMN Lab 2: Add A Rule To An Existing Decision Table

. Go back to the asset view for the FraudManagement-DMN project
. Select the "Fraud Scoring Basic" DMN model. The DMN model should now appear and a palette containing six DMN design icons should appear towards the left of the screen.
.

==== DMN Lab 3: Add New Rule Constraint 

. Navigate to the DMN Model
.. Go back to the asset view for the FraudManagement-DMN project
.. Select the "Fraud Scoring Basic" DMN model. The DMN model should now appear
.. A palette containing six DMN design icons should appear towards the left of the screen. 
. Add a new "DMN Decision" 
.. Clicking on the rectangular shaped icon in the design palette
.. Name the new DMN decision "Total Amount from Last 24 hours Transactions"
+
.Add new DMN Decision
image::docs/images/dmn-model-decision-add.png[Overview, 750,380]
+
. Configure the rule
.. Click on the edit icon just to the left of the rectangular box. The edit icon should have a pen next to it.
.. Click in the "Select expression" field and choose "Context" from the list
+
.Select expression
image::docs/images/dmn-model-decision-select-context.png[Overview, 700,202]
+
. Add a new row by right clicking on the first row
+
.Insert new row
image::docs/images/dmn-model-decision-insert-row.png[Overview, 700,261]
+
. Select the expression type by clicking on the words "Select expression" and choose "Literal expression".
+
.Select expression
image::docs/images/dmn-model-decision-select-expression.png[Overview, 700,284]
+
. Click on the "ContextEntry-1" on the first row and set the following values:  
.. Set *Name*: last 24 hours transactions +
.. Set *DataType*: tTransactions +
. Click on the "Select expression" field and select "Literal Expression" from the Select logic type list.
.. Now enter the following information into the cell:
... Transactions[ duration(Last Transaction.Date - Date) <= duration("PT24H")]
+
NOTE: Boxed expressions in DMN are tables that you use to define the underlying logic of decision nodes and business knowledge models in a decision requirements diagram (DRD) or decision requirements graph (DRG). Some boxed expressions can contain other boxed expressions, but the top-level boxed expression corresponds to the decision logic of a single DRD artifact. A boxed literal expression in DMN is a literal FEEL expression as text in a table cell, typically with a labeled column and an assigned data type. You use boxed literal expressions to define simple or complex node logic or decision data directly in FEEL for a particular node in a decision +
+
. Add a second row by right clicking anywhere in the first row and selecting "Insert below"+
.. Click on the "ContextEntry-1" on the second row and set the following values:  
... Set *Name* : "last 24 hours amount" 
... Set *DataType*: undefined
. Click on the "Select expression" field and select "Literal Expression" from the Select logic type list.
.. Now enter the following information into the cell:
.. 'for tnx in last 24 hours transactions return tnx.Amount'
+
. Complete the rule by adding a final result expression. 
.. Click the "Select Expression" text next to where it says "<result>". +
.. Select "Literal expression" and enter the following :
... sum (last 24 hours amounts)
+
. The completed table should look similar to this. +
+
.Complete rule view
image::docs/images/DMN-Context-Rule-Solution.png[Overview, 700,243]
+
. Save the changes 
.. Click save
.. Click the "Back to Fraud-Scoring" link +
+
. Connect the new decision node with the "Risk Score" rule.
.. Click on the new decision you've just created and select the solid arrow option that appears on the right hand side. When you hover over the arrow the following information label should appear "Create DMN Information Requirement".
.. Drag the line to the "Risk Score" Decision rectangle.
.. Select the edit link on the "Risk Score" Decision node.
.. A context expression should appear containing four rows. 
.. Right click anywhere along the number column and select the "Insert below" option.
+
.Insert new row
image::docs/images/dmn-model-risk-score-insert-row.png[Overview, 700,283]
+
.. Click where it says "ContextEntry-1" and set the following values:
... Set *Name* :  "Last 24 hours Transactions Score" 
... Set *DataType*: "tRiskScore" 
. A new rule input has now been added and the result from this needs to be taken into consideration. 
.. Find the last row of the table where it says "<result>".
+
.Modify risk score result
image::docs/images/dmn-model-risk-score-rule-new.png[Overview, 700,296]
+
.. Click in the cell to the right that contains the following function :
+
----
max([Blacklisted Marchant Score, Last Transaction Amount Score, Denied Transactions Score ])
----
+
.. Modify the max fuction by adding the result from the "Last 24 hours Transactions Score". The function should look as follows.
+
----
max([Blacklisted Marchant Score, Last Transaction Amount Score, Denied Transactions Score, *Last 24 hours Transactions Score* ])
----
+
.Final Risk Score Rule Configuration
image::docs/images/dmn-model-risk-score-result.png[Overview, 700,345]
+
. Save the changes and return to the project asset page
.. Click Save
.. Click the "Back to Fraud-Scoring" link.
. Deploy and Test the Changes
.. Test the new rule by selecting the "FraudManagement-DMN" link which will take you back to the main project assest screen.
.. Click the "Build" button to recompile the rule
.. Click the deploy button to deploy the DMN rule to the local runtime environment known as the kie-server.
+
.Build and Deploy
image::docs/images/dmn-model-build-deploy.png[Overview, 700,225]
+
.. Finally, click the "Test" button to execute all of the tests contained within the "Test-Scenario" asset.
.. When you click test a "Test Results" pane should appear on the right handside of the screen.






